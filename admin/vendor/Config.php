<?php 

	/**
	* v.1.1
	* Getting configuration and connection to database
	*/
	class Config {
		private static $config; // saved config
		
		/*
		* Start application with config
		* If exests configuration db will traed connecting to database
		*/
		public static function run() {
			if (empty(self::$config)) {
				$config = include BASE.'config/app.php';
				if (isset($config['db']) AND !empty($config['db'])) {
					include_once APP.'DB.php';
					DB::connect($config['db']);
					unset($config['db']);
				}				
				self::$config = $config;
			}
		}

		/*
		* getting any configuration attribute
		*/
		public static function get($value = '') {
			if (!empty($value)) {
				return isset(self::$config[$value]) ? self::$config[$value] : '';
			}
			else{
				throw new Exception("get({value}): value can not be empty", 1);
			}
		}

		/*
		* getting any params in configuration
		*/
		public static function params($value) {
			$params = self::get('params');
			if (!empty($params) AND isset($params[$value])) {
				return $params[$value];
			}
			return '';
		}

		/*
		* Will check route in databse created dinamic pages
		* Return array
		*/
		public static function getThisFromDB($route){
			$route = str_replace('/', '.', $route);
			$pagetable = self::get('pagetable');
			if (DB::getStatus() > 0 AND !empty($pagetable)) {
				$res = DB::one("SELECT * FROM {$pagetable} WHERE url = :url", [':url' => $route]);
				return $res;
			}
			return [];
		}
	}