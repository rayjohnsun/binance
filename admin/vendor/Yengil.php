<?php 

	/**
	* v1.1
	* Main class for this application
	*/
	class Yengil {
		
		public static $controller_id; // saved controller_name
		public static $action_id; // saved action_name
		public static $params; // saved others parameters from url
		private static $view_file; // view file
		private static $variables; // variables in view file
		private static $default_flash = 'flash_text';

		private static $cssFiles = [];
		private static $jsFiles = [];

		public static function getHeader() {
			if (!empty(self::$cssFiles)) {
				foreach (self::$cssFiles as $value) {
					echo "<link rel=\"stylesheet\" href=\"{$value}\">";
				}
			}
		}

		public static function getFooter() {
			if (!empty(self::$jsFiles)) {
				foreach (self::$jsFiles as $value) {
					echo "<script src=\"{$value}\"></script>";
				}
			}
		}

		public static function setcss($value) {
			self::$cssFiles[] = $value;
		}

		public static function setjs($value) {
			self::$jsFiles[] = $value;
		}

		public static function getModule($value) {
			if (!empty($value)) {
				$plugins = Config::get('plugins');
				if (!empty($plugins) AND is_array($plugins)) {
					if (isset($plugins[$value])) {
						$path 	= toUrl($value);
						$plugin = $plugins[$value];
						$file 	= PLUGINS.$path.$plugin.'.php';
						if (file_exists($file)) {
							include_once $file;
							$new_plugin = new $plugin();
							if (method_exists($new_plugin, 'run')) {
								$new_plugin->run();
							}
							return $new_plugin;
						}
						else{throw new Exception("Plugin: `{$plugin}` should be installed as: `plugins/{$path}{$plugin}.php`", 1); }
					}
					else{throw new Exception("Called plugin: `{$value}` is not found", 1);}
				}
				else{throw new Exception("No added plugins in config as: `plugins` => [`dir_name` => `class_name`]", 1);}
			}
			else{
				throw new Exception("Called: `Yengil::require($a)` where `$a` can not be empty", 1);
			}
			return false;
		}

		/*
		* Setting view file and variables
		*/
		public static function setView($view, $variables) {
			self::$view_file = $view;
			self::$variables = $variables;
		}

		/*
		* Setting view file and variables
		*/
		public static function includeView($view, $variables) {
			if (!empty($variables)) {
				foreach ($variables as $key => $value) {
					${$key} = $value;
				}
				include $view;
				foreach ($variables as $key => $value) {
					unset(${$key});
				}
			}
			else{
				include $view;
			}
		}

		/*
		* including view file and generating variables
		*/
		public static function getContent() {
			$message = Yengil::hasFlash() ? Yengil::getFlash() : '';
			if (!empty(self::$variables)) {
				foreach (self::$variables as $key => $value) { // generating variables
					${$key} = $value;
				}
				include_once self::$view_file;
				foreach (self::$variables as $key => $value) { // deleting variables
					unset(${$key});
				}
			}
			else{
				include_once self::$view_file;
			}
		}

		/*
		* Getting current user
		*/
		public static function user($value='') {
			if (isset($_SESSION['login_user']) AND !empty($value)) {
				if (isset($_SESSION['login_user'][$value])) {
					return $_SESSION['login_user'][$value];
				}
			}
			return NULL;
		}

		public static function issetUser() {
			return (isset($_SESSION['login_user']) AND !empty($_SESSION['login_user']));
		}

		/*
		* Login user
		*/
		public static function auth($user) {
			$_SESSION['login_user'] = $user;
		}

		/*
		* Remove user
		*/
		public static function removeUser() {
			if (isset($_SESSION['login_user'])) {
				unset($_SESSION['login_user']);
			}
		}

		/*
		* Setting alert message
		*/
		public static function flash($text, $status=0, $flash_name='') {
			$status_text = $status > 0 ? 'success' : 'danger';
			if (!empty($flash_name) AND $flash_name != 'login_user') {
				$_SESSION[$flash_name] = [$text, $status_text];
			}
			else{
				$_SESSION[self::$default_flash] = [$text, $status_text];
			}
		}

		/*
		* Getting alert message
		*/
		public static function postFlash($flash_name='') {
			$fl = ['', ''];
			if (!empty($flash_name) AND $flash_name != 'login_user') {
				if(isset($_SESSION[$flash_name])){
					$fl = $_SESSION[$flash_name];
				}
			}
			else{
				if(isset($_SESSION[self::$default_flash])){
					$fl = $_SESSION[self::$default_flash];
				}
			}
			return $fl;
		}

		/*
		* Getting alert message
		*/
		public static function getFlash($flash_name='') {
			$fl = ['', ''];
			if (!empty($flash_name) AND $flash_name != 'login_user') {
				if(isset($_SESSION[$flash_name])){
					$fl = $_SESSION[$flash_name];
					unset($_SESSION[$flash_name]);
				}
			}
			else{
				if(isset($_SESSION[self::$default_flash])){
					$fl = $_SESSION[self::$default_flash];
					unset($_SESSION[self::$default_flash]);
				}
			}
			return $fl;
		}

		/*
		* Checking alert message
		*/
		public static function hasFlash($flash_name='') {
			if (!empty($flash_name) AND $flash_name != 'login_user') {
				return isset($_SESSION[$flash_name]);
			}
			else{
				return isset($_SESSION[self::$default_flash]);
			}
		}
	}