<?php 

	/**
	* v.1.1
	* For opirating with database
	*/
	class DB {
		public static $pdo;
		protected static $status = 0;

		/*Connection*/
		public static function connect($db) {
			try {
				$dsn = "mysql:host=".$db['host'].";dbname=".$db['db'].";charset=".$db['charset'];
				$opt = [
			        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			        PDO::ATTR_EMULATE_PREPARES   => false,
			    ];
		    	self::$pdo = new PDO($dsn, $db['user'], $db['pass'], $opt);
		    	self::$status = 1;
			} 
			catch (PDOException $e) {
			    die('Can not connect to database: ' . $e->getMessage());
			}
		}

		/*
		* Execute sql queries: INSERT, UPDATE, DELETE
		* Reteurned true or false
		*/
		public static function prepare($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {
				if ($stmt->execute($params)) {
					return true;
				}
				else{throw new Exception($stmt->errorInfo(), 1);}
			}
			else{
				if ($stmt->execute()) {
					return true;
				}
				else{throw new Exception($stmt->errorInfo(), 1);}
			}
		}

		/*
		* Execute sql query: SELECT
		* Returned all result in array
		*/
		public static function all($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {$stmt->execute($params);}
			else{$stmt->execute();}
			$result = [];
			while ($row = $stmt->fetch()) {
				$result[] = $row;
			}
			return $result;
		}

		/*
		* Execute sql query: SELECT
		* Returned one result in array
		*/
		public static function one($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {$stmt->execute($params);}
			else{$stmt->execute();}
			$result = $stmt->fetch();
			return !empty($result) ? $result : [];
		}

		/*
		* Execute sql query: SELECT COUNT(*)
		* Returned number of result | Executed sql if not found `cnt`
		*/
		public static function count($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {$stmt->execute($params);}
			else{$stmt->execute();}
			$result = $stmt->fetch();
			return isset($result['cnt']) ? $result['cnt'] : $result;
		}

		/*
		* Get status connection
		*/
		public static function getStatus() {
			return self::$status;
		}
	}