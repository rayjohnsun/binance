<?php 

	/**
	* v.1.1
	* Routing and connection to controllers
	*/
	class Router {

		protected $controller_id; // Saved controller_name
		protected $action_id; // Saved action name
		protected $params; // Saved another parameters in URL

		/*
		* Getting Controller, Action and params
		*/
		function __construct() {
			$this->controller_id = 'main'; // Default sontroller: MainController
			$this->action_id = 'index'; // Default action: ActionIndex()
			$this->params = []; // Default params: empty array
			if (isset($_GET['params'])) {
				$params = trim($_GET['params']);
				$params = trim($params, '\\');
				$params = trim($params, '/');
				unset($_GET['params']);

				$dbparams = Config::getThisFromDB($params);
				if (!empty($dbparams)) {
					$this->controller_id = 'dinamic';
					$this->params = [$dbparams];
				}
				else{
					$paths = explode('/', $params);
					$this->controller_id = htmlspecialchars(array_shift($paths));
					if (!empty($paths)) {
						$this->action_id = htmlspecialchars(array_shift($paths));
					}
					$this->params = $paths;
				}
			}
		}

		/*
		* Call to Controller and call to action
		* Saving their to Main Class Yengil
		* Autoloading classes from common
		*/
		public function run() {
			include_once APP.'Controller.php';
			$path 			= WEB.'controllers/';
			$controller 	= $this->getController();
			if (!file_exists($path.$controller.'.php')) {
				$this->controller_id = 'main';
			}
			$controller 	= $this->getController();
			include_once $path.$controller.'.php';
			$cont = new $controller();
			$action = $this->getAction();
			if (!method_exists($cont, $action)) {
				$this->action_id = 'index';
			}
			$action = $this->getAction();
			Yengil::$controller_id = $this->controller_id;
			Yengil::$action_id = $this->action_id;
			Yengil::$params = $this->params;
			$p = [];
			for ($i=0; $i < 10; $i++) { 
				$p[$i] = isset($this->params[$i]) ? $this->params[$i] : null; // Auto generating 10 arguments for Action
			}
			$this->loadCommon();
			$cont->$action($p[0],$p[1],$p[2],$p[3],$p[4],$p[5],$p[6],$p[7],$p[8],$p[9]);
			return true;
		}

		/*
		* Auto load classes from common
		*/
		public function loadCommon() {
			function __autoload($class_name) {
				$file_name = COMMON.$class_name.'.php';
				include_once $file_name;
			}
		}

		/*
		* Generating Controller name
		*/
		protected function getController() {
			return ucfirst($this->controller_id).'Controller';
		}

		/*
		* Generating Action name
		*/
		protected function getAction() {
			return 'action'.ucfirst($this->action_id);
		}
	}