<?php 

	function htmlDecode($value='') {
		if (!empty($value)) {
			if (is_array($value)) {
				$value2 = [];
				foreach ($value as $key => $val) {
					$value2[$key] = htmlspecialchars_decode($val);
				}
				return $value2;
			}
			else{
				return htmlspecialchars_decode($value);
			}
		}
		return $value;
	}
	
	function toUrl($value='') {
		$value = trim($value);
		$value = trim($value, '/');
		$value = trim($value, '\\');
		$value = str_replace('.', '/', $value);
		return $value . '/';
	}

	function issetPost($value) {
 		return isset($_POST[$value]);
	}
	
	function Post($value) {
		if (isset($_POST[$value])) {
			return $_POST[$value];
		}
		return '';
	}
	
	function issetGet($value) {
 		return isset($_GET[$value]);
	}
	
	function Get($value) {
		if (isset($_GET[$value])) {
			return $_GET[$value];
		}
		return '';
	}

	function val($arr, $val) {
		if (isset($arr[$val])) {
			return htmlspecialchars($arr[$val]);
		}
		return '';
	}

	/*Beautiful show variables for developers dd($a); ddv($a); kk($a); kkv($a);*/
	function dd($value='') {
		echo "<pre>";
		var_export($value);
		echo "</pre>";
		exit();
	}

	function ddv($value='') {
		echo "<pre>";
		var_export($value);
		echo "</pre>";
	}

	function kk($value='') {
		echo "<pre>";
		var_dump($value);
		echo "</pre>";
		exit();
	}

	function kkv($value='') {
		echo "<pre>";
		var_dump($value);
		echo "</pre>";
	}
	/*------------------------------------------------------------*/

	function redirect($value='/') {
		header('Location: /admin'.$value);
		exit;
	}

	function sendEmail($email, $subject, $params) {
		$message = "Спасибо за регистрацию, ваши регистрационные данные<br><br><br>";
		if (!empty($params)) {
			foreach ($params as $key => $value) {
				$message .= "<b>".$key."</b> ".$value."<br><br>";
			}
		}
      	$headers = array(
        	'From: noreply@email.ru',
        	'Reply-To: noreply@email.ru',
        	'X-Mailer: PHP/' . phpversion(),
        	'MIME-Version: 1.0',
        	'Content-type: text/html; charset=utf-8'
      	);
      	$headers = implode("\r\n", $headers);      
      	$status = mail("245464786@mail.ru", $subject, $message, $headers);
		return $status;
	}

	/**********Ctrypto functions*******************/

	function getImageName($code, $img = '') {
		if (!empty($img)) {
			$code 	= str_replace('*', '_', $code);
			$code 	= trim($code);
			$code 	= strtolower($code);
			$ext 	= '.png';
			$exts 	= explode('.', $img);
			$ext 	= end($exts);
			$ext 	= '.' . $ext;
			$code 	= $code . $ext;
			return $code;
		}
		return NULL;
	}

	function showErrors() {
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	function get_url_content($url, $param = false){
	    $c = curl_init();
	    curl_setopt($c, CURLOPT_URL, $url);
	    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($c, CURLOPT_AUTOREFERER, true);
	    curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
	    $data = curl_exec($c);
	    curl_close($c);
	    return json_decode($data, $param);  
	}