<?php 

	/**
	* v1.1
	* Parent Controller, all controllers should be extend of this
	*/
	class Controller {

		public $title = 'Document';
		public $layout = 'layout'; // Default layout
		
		/*
		* render('view_file_name', [array])
		* In result included layout and view
		*/
		public function render($value, $variables=[]) {
			$view = WEB.'views/'.Yengil::$controller_id.'/'.$value.'.php';
			$layout = WEB.'views/'.$this->layout.'.php';
			if (file_exists($view)) {
				Yengil::setView($view, $variables); // include view and generate variables 
				include_once $layout;
			}
			else{
				throw new Exception("View file not found {$value}.php", 1);
			}
		}
		
		/*
		* render('view_file_name', [array])
		* In result included layout and view
		*/
		public function renderPartial($value, $variables=[]) {
			$view = WEB.'views/'.Yengil::$controller_id.'/'.$value.'.php';
			if (file_exists($view)) {
				Yengil::includeView($view, $variables); // include view and generate variables 
			}
			else{
				throw new Exception("View file not found {$value}.php", 1);
			}
		}

		/*
		* Check is guest and redirect to login
		*/
		public function authorise($login = true) {
			if (!Yengil::issetUser()) {
				$login == true ? redirect('/login') : redirect('/');
			}
		}

		/*
		* Check is user and redirect to index
		*/
		public function unauthorise() {
			if (Yengil::issetUser()) {
				redirect('/login');
			}
		}
	}