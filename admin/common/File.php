<?php 

	/**
	* v.1.1
	* For uploading files
	*/
	class File {
		public $url = '';
		protected $target_dir = "uploads/";
		public $allowed = ['png', 'gif', 'jpg'];
		
		public function upload($name) {
			$target_dir = $this->target_dir;
			if (isset($_FILES[$name]) AND !empty($_FILES[$name]['tmp_name'])) {
				$target_file = $target_dir . basename($_FILES[$name]["name"]);
				$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
				if (in_array($imageFileType, $this->allowed)) {
					$new_target_file = $target_dir.uniqid().time().'.'.$imageFileType;
					$check = getimagesize($_FILES[$name]["tmp_name"]);
					if ($check !== false) {
						if (move_uploaded_file($_FILES[$name]["tmp_name"], $new_target_file)) {
							$this->url = $new_target_file;
						}
					}
					else{
						Yengil::flash('Thomething went wrong, try again please', 0);
					}
				}
				else{
					Yengil::flash('Allowed formats: jpg, gif, png', 0);
				}
			}
			else{
				Yengil::flash('Choose plese image file', 0);
			}
		}

		public function delete() {
			$file = $this->target_dir.$this->url;
			if (file_exists($file)) {
				unlink($file);
			}
		}
	}