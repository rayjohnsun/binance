<?php 

	class LogoutController extends Controller {
		
		public function actionIndex() {

			$this->authorise();

			Yengil::removeUser();

			redirect('/');

		}
		
	}