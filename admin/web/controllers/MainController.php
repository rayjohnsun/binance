<?php 

	/**
	* This is default Controller
	*/
	class MainController extends Controller {
		
		public function actionIndex() {


			$this->layout = 'admin';

			$this->authorise();

			Yengil::getModule('bootstrap');

			$this->render('index', [

				'message' => Yengil::hasFlash() ? Yengil::getFlash() : '',

			]);

		}

	}