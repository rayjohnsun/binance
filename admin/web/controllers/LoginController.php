<?php 

	class LoginController extends Controller {
		
		public function actionIndex() {

			$this->layout = 'look';

			$this->unauthorise();

			Yengil::getModule('bootstrap');

			$user = [];

			if (!empty(Post('User'))) {

				$user = Post('User');

				$account = DB::one("SELECT id, login, fio FROM users WHERE login = :login AND password = :pass", [':login' => val($user, 'login'), ':pass' => sha1(val($user, 'password'))]);

				if (!empty($account)) {

					Yengil::auth($account);

					redirect('/main');

				}

				else{

					Yengil::flash('Login or password not correct', 0);

				}

			}

			$this->render('login_form', [

				'user' => $user,

				'message' => Yengil::hasFlash() ? Yengil::getFlash() : '',

			]);
		}
	}