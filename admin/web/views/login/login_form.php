<div class="row">
	<div class="col-sm-4">&nbsp;</div>
	<div class="col-sm-4" style="margin-top: 40px;">
		<?php if (!empty($message)): ?>
			<div class="alert alert-<?=$message[1] ?>">
				<span><?=$message[0] ?></span>
			</div>
		<?php endif ?>
		<form action="<?=PATH ?>admin/login" method="POST">
			<div class="form-group">
				<label for="Login">Login</label>
				<input type="text" class="form-control" id="Login" name="User[login]" value="<?=val($user, 'login') ?>">
			</div>
			<div class="form-group">
				<label for="Password">Password</label>
				<input type="password" class="form-control" id="Password" name="User[password]" value="<?=val($user, 'password') ?>">
			</div>
			<button type="submit" class="btn btn-primary">Login</button>
			<a href="/" style="margin-left: 20px;">Go to Home page</a>
		</form>
	</div>
	<div class="col-sm-4">&nbsp;</div>
</div>