<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$this->title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php Yengil::getHeader() ?>
</head>
<body>
	<div class="container">
		<?=Yengil::getContent() ?>
	</div>
	<script src="<?=PATH ?>admin/theme/js/jquery-3.3.1.min.js"></script>
	<script src="<?=PATH ?>admin/theme/js/script.js"></script>
</body>
</html>