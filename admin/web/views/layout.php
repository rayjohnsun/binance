<!DOCTYPE html>
<html lang="ru-Ru">
<head>
	<meta charset="UTF-8">
	<title><?=$this->title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="msapplication-tap-highlight" content="no">
  <?php Yengil::getHeader() ?>
	<link rel="stylesheet" href="<?=PATH ?>admin/theme/css/style.css">
	<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script><script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]--><!--[if (gte IE 6)&(lte IE 8)]> <script src="<?=PATH ?>admin/theme/js/selectivizr-min.js"></script><![endif]-->
	<script src="<?=PATH ?>admin/theme/js/jquery-3.3.1.min.js"></script>
  <?php Yengil::getFooter() ?>
</head>
<body>
	<div class="all_container">
    <div id="content">
  		<? Yengil::getContent() ?>
    </div>
	</div>
</body>
</html>