<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$this->title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php Yengil::getHeader() ?>
	<link rel="stylesheet" href="<?=PATH ?>admin/theme/css/admin.css">
	<script src="<?=PATH ?>admin/theme/js/jquery-3.3.1.min.js"></script>
	<?php Yengil::getFooter() ?>
	<script src="<?=PATH ?>admin/theme/js/script.js"></script>
</head>
<body>
	<div class="container">
		<div class="header">
			<ul class="nav h_menu">
				<li class="nav-item <?=Yengil::$controller_id=='settings'?'active':'' ?>">
					<a href="<?=PATH ?>admin/main" class="nav-link">Главная</a>
				</li>
				<li class="nav-item <?=(Yengil::$controller_id=='coins' AND Yengil::$action_id == 'default')?'active':'' ?>">
					<a href="<?=PATH ?>admin/pages" class="nav-link">Страницы</a>
				</li>
			</ul>
			<ul class="nav">
				<li class="nav-item">
					<a href="<?=PATH ?>admin/logout" class="nav-link">Выход</a>
				</li>
			</ul>
		</div>
		<div class="body">
			<?php Yengil::getContent() ?>
		</div>
	</div>
</body>
</html>