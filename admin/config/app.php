<?php 
	define('PATH', '/');
	$db = include_once dirname(__FILE__).'/db.php';
	return [
		'db' => $db,
		'plugins' => [
			'bootstrap' 	=> 'Bootstrap',
			'summernote'	=> 'Summernote',
			'coinlist' 		=> 'Coinlist',
			'tablesorter' 	=> 'Tablesorter',
			'cryptocompare' => 'Cryptocompare',
		],
	];