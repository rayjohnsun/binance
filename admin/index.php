<?php 
	session_start();

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	define('KOREN',dirname(__FILE__).'/../');
	define('BASE',dirname(__FILE__).'/');
	define('APP', dirname(__FILE__).'/vendor/');
	define('WEB', dirname(__FILE__).'/web/');
	define('COMMON', dirname(__FILE__).'/common/');
	define('PLUGINS', dirname(__FILE__).'/plugins/');

	include_once APP.'Functions.php';
	include_once APP.'Yengil.php';
	include_once APP.'Config.php';
	Config::run();
	include_once APP.'Router.php';
	$router = new Router();
	$router->run();

