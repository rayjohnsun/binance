<?php 

	/**
	* v.1.1.1
	*/
	class Bootstrap {
		
		function __construct() {}

		public function run() {
			Yengil::setcss(PATH."admin/plugins/bootstrap/bootstrap3.min.css");
			Yengil::setjs(PATH."admin/plugins/bootstrap/bootstrap3.min.js");
		}
	}