<?php 

	/**
	* v 1.0
	*/
	class Cryptocompare {

		protected $ch;
		protected $api_start;
		protected $api_start2;
		protected $full_api;
		public $api;
		
		function __construct() {}
		
		public function run() {
			$this->api_start = 'https://min-api.cryptocompare.com/';
			$this->api_start2= 'https://api.coinmarketcap.com/v1/ticker/?limit=10000';
		}

		public function init() {
			if (!empty($this->api)) {
				$this->full_api = $this->api_start.$this->api;
			}
		}

		public function init2() {
			$this->full_api = $this->api_start2;
		}


		public function open() {
			$this->ch = curl_init();
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
		    curl_setopt($this->ch, CURLOPT_ENCODING, 'gzip,deflate');
		}

		public function all() {
			if (!empty($this->full_api)) {
				curl_setopt($this->ch, CURLOPT_URL, $this->full_api);
				$result = curl_exec($this->ch);
				return json_decode($result, true);
			}
			throw new Exception("full_api is missing", 1);
		}

		public function close() {
			curl_close($this->ch);
		}
	}