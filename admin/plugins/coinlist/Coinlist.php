<?php 

	/**
	* v1.1.1
	*/
	class Coinlist {

		public $list;
		public $iconurl;
		public $time_file;
		public $refresh_time;
		public $coins_file;
		public $coins_array;
		public $raw;
		
		function __construct() {}

		public function run() {
			$this->coins_file = KOREN.'refresh/list.php';
			$this->iconurl = PATH.'theme/images/coinssmall/';
			$this->list = include $this->coins_file;
			// $this->time_file = dirname(__FILE__).'/time.txt';
			// $this->refresh_time = '1 day';
		}

		public function getCoin($value) {
			if (isset($this->list[$value])) {
				return $this->list[$value];
			}
			return '';
		}

		// public function getRawDisplay() {
		// 	if (!empty($this->raw)) {
		// 		if (array_key_exists('RAW', $this->raw)) {
		// 			$raw = array_shift($this->raw['RAW']);
		// 			$display = array_shift($this->raw['DISPLAY']);
		// 			if (!empty($raw)) {
		// 				$data = [];
		// 				foreach ($raw as $cur => $a_raw) {
		// 					$a_display = $display[$cur];
		// 					$data[$cur]['price'] 	= @$a_display['PRICE'];
		// 					$data[$cur]['price2']	= @$a_raw['PRICE'];
		// 					$data[$cur]['from_symbol']= @$a_display['FROMSYMBOL'];
		// 					$data[$cur]['to_symbol']= @$a_display['TOSYMBOL'];
		// 					$data[$cur]['cap']		= number_format((float)@$a_raw['MKTCAP'],2,'.',',');
		// 					$data[$cur]['cap2']		= (float)@$a_raw['MKTCAP'];
		// 					$data[$cur]['obem']		= @$a_display['VOLUME24HOURTO'];
		// 					$data[$cur]['obem2']	= @$a_raw['VOLUME24HOURTO'];
		// 					$data[$cur]['vipushen']	= @$a_display['SUPPLY'];
		// 					$data[$cur]['vipushen2']= @$a_raw['SUPPLY'];
		// 					$data[$cur]['24ch']		= number_format((float)@$a_display['CHANGEPCT24HOUR'],2,'.','');
		// 					$data[$cur]['7d']		= number_format((float)@$a_display['CHANGEPCTDAY'],2,'.','');
		// 				}
		// 				return $data;
		// 			}
		// 			return [];
		// 		}
		// 		return [];
		// 	}
		// 	throw new Exception("raw is missing", 1);
		// }

		// public function write() {
		// 	$fp = fopen($this->coins_file, 'w');
		// 	fwrite($fp, "<?php\nreturn\t");
		// 	fwrite($fp, var_export($this->coins_array, true));
		// 	fwrite($fp, ";");
		// 	fclose($fp);
		// 	return true;
		// }

		// public function getTime() {
		// 	$time = (int)file_get_contents($this->time_file);
		// 	return $time;
		// }

		// public function refreshTime() {
		// 	$time = strtotime('+'.$this->refresh_time);
		// 	$fh = fopen($this->time_file, 'w');
		// 	fwrite($fh, $time);
		// 	fclose($fh);
		// }

		
	}