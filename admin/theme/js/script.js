function loadImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#LoadedImage').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

// $(document).ready(function () {
// 	$('.view_image').click(function () {
// 		var img = $(this).clone();
// 		var p = $('#Popup');
// 		if (p.length > 0) {
// 			p.show();
// 			p.find('.cont').html(img);
// 		}
// 	});
// });
	
// $(document).on('click', '#Popup', function () {
// 	$('#Popup').hide();
// });

