<?php 
	function kkv($value='') {
		echo "<pre>";
		var_dump($value);
		echo "</pre>";
	}

	function kk($value='') {
		echo "<pre>";
		var_dump($value);
		echo "</pre>";
		exit;
	}

	function ddv($value='') {
		echo "<pre>";
		var_export($value);
		echo "</pre>";
	}

	function dd($value='') {
		echo "<pre>";
		var_export($value);
		echo "</pre>";
		exit;
	}

	function requestIsAjax() {
    	return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND !empty($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

	function getIp() {
		$ip = ''; 
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {$ip = $_SERVER['HTTP_X_FORWARDED_FOR']; }
        if (isset($_SERVER['HTTP_CLIENT_IP'])) { $ip = $_SERVER['HTTP_CLIENT_IP']; }
        if (isset($_SERVER['REMOTE_ADDR'])) { $ip = $_SERVER['REMOTE_ADDR']; }
        if (isset($_SERVER['HTTP_X_REAL_IP'])) { $ip = $_SERVER['HTTP_X_REAL_IP']; }
        return $ip; 
	}
	
	function domain() {
		return isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
	}

	function curr_file() {
		return basename($_SERVER['PHP_SELF']);
	}

	function tryGetMonthName($num) {
		$num = (int)$num;
		$months = [
			1 	=> 'January',
			2 	=> 'February',
			3 	=> 'March',
			4 	=> 'April',
			5 	=> 'May',
			6 	=> 'June',
			7 	=> 'July',
			8 	=> 'August',
			9 	=> 'September',
			10 	=> 'October',
			11 	=> 'November',
			12 	=> 'December',
		];
		if (isset($months[$num])) {
			return $months[$num];
		}
		return $num;
	}