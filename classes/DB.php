<?php 

	/**
	* v.1.1
	*/
	class DB {
		public static $pdo;
		
		public static function connect($db) {
			try {
				$dsn = "mysql:host=".$db['host'].";dbname=".$db['db'].";charset=".$db['charset'];
				$opt = [
			        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			        PDO::ATTR_EMULATE_PREPARES   => false,
			    ];
		    	self::$pdo = new PDO($dsn, $db['user'], $db['pass'], $opt);
			} 
			catch (PDOException $e) {
			    die('Подключение не удалось: ' . $e->getMessage());
			}
		}

		public static function prepare($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {
				return $stmt->execute($params);
			}
			return $stmt->execute();
		}

		public static function all($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {$stmt->execute($params);}
			else{$stmt->execute();}
			$result = [];
			while ($row = $stmt->fetch()) {
				$result[] = $row;
			}
			return $result;
		}

		public static function one($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {$stmt->execute($params);}
			else{$stmt->execute();}
			$result = $stmt->fetch();
			return !empty($result) ? $result : [];
		}

		public static function count($sql, $params = []) {
			$stmt = self::$pdo->prepare($sql);
			if (!empty($params)) {$stmt->execute($params);}
			else{$stmt->execute();}
			$result = $stmt->fetch();
			return isset($result['cnt']) ? $result['cnt'] : $result;
		}
	}