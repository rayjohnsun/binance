<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>The page you requested could not be found: error 404 | Binance referral</title>
	<meta name="keywords" content="error 404, Page not found Binance referral">
	<meta name="description" content="The page you requested could not be found: error 404">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
	<style>
		body{
			background: #E8EAFF;
		}
		h4{
			color: #2f2886;
		    font: bold 60px/80px 'Montserrat';
		    text-transform: uppercase;
		    margin-bottom: 20px;
		    text-align: center;
		    margin-top: 100px;
		}
		p{
			color: #2f2886;
		    font: bold 15px/19px 'Montserrat';
		    margin-bottom: 20px;
		    text-align: center;
		}
		a{
			color: blue;
		}
	</style>
</head>
<body>
	<h4>404</h4>
	<p>he page you requested could not be found Please start searching for the information you need by beginning from the <a href="/">main</a> page of the site</p>
</body>
</html>