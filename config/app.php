<?php 
	
	$db = include_once dirname(__FILE__).'/db.php';
	return [
		'db' => $db,
		'commission'=> ['from' => 0.001,	'to' => 0.08], // можно дробной число
		'peoples'	=> ['from' => 5, 		'to' => 50], // только целой число
		'trader'	=> ['from' => 300000,	'to' => 500000], // только целой число
		'list_coin' => [
			'BTC' 	=> ['id' => 'Bitcoin', 				'icon' => 'btc.png'],
			'ETH' 	=> ['id' => 'Ethereum', 			'icon' => 'eth.png'],
			'EOS' 	=> ['id' => 'EOS', 					'icon' => 'eos.png'],
			'BCH' 	=> ['id' => 'Bitcoin Cash / BCC', 	'icon' => 'bch.jpg'],
			'XRP' 	=> ['id' => 'Ripple', 				'icon' => 'xrp.png'],
			'LTC' 	=> ['id' => 'Litecoin', 			'icon' => 'ltc.png'],
			'TRX' 	=> ['id' => 'Tronix', 				'icon' => 'trx.png'],
			'HT' 	=> ['id' => 'Huobi Token', 			'icon' => 'ht.png'],
			'ETC' 	=> ['id' => 'Ethereum Classic', 	'icon' => 'etc.png'],
			'BNB'	=> ['id' => 'Binance Coin', 		'icon' => 'bnb.png'],	
			'XVG' 	=> ['id' => 'Verge', 				'icon' => 'xvg.png'],
			'NEO' 	=> ['id' => 'NEO', 					'icon' => 'neo.jpg'],
			'DASH' 	=> ['id' => 'DigitalCash', 			'icon' => 'dash.png'],
			'QTUM' 	=> ['id' => 'QTUM', 				'icon' => 'qtum.png'],
			'ICX' 	=> ['id' => 'ICON Project', 		'icon' => 'icx.png'],
			'ONT' 	=> ['id' => 'Ontology', 			'icon' => 'ont.jpg'],
			'IOT' 	=> ['id' => 'IOTA', 				'icon' => 'iot.png'],
			'XMR' 	=> ['id' => 'Monero', 				'icon' => 'xmr.png'],
			'XLM' 	=> ['id' => 'Stellar', 				'icon' => 'xlm.png'],
			'VEN' 	=> ['id' => 'Vechain', 				'icon' => 'ven.png'],
			'ZEC' 	=> ['id' => 'ZCash', 				'icon' => 'zec.png'],
			'IOST' 	=> ['id' => 'IOS token', 			'icon' => 'iost.png'],
			'ADA' 	=> ['id' => 'Cardano', 				'icon' => 'ada.png'],
			'ELF' 	=> ['id' => 'aelf', 				'icon' => 'elf.png'],
			'ABT' 	=> ['id' => 'ArcBlock', 			'icon' => 'abt.png'],
			'OCN' 	=> ['id' => 'Odyssey', 				'icon' => 'ocn.png'],
			'BTM*' 	=> ['id' => 'Bytom', 				'icon' => 'btm_.png'],
			'WAVES' => ['id' => 'Waves', 				'icon' => 'waves.png'],
			'OMG' 	=> ['id' => 'OmiseGo', 				'icon' => 'omg.png'],
			'DGD' 	=> ['id' => 'Digix DAO', 			'icon' => 'dgd.png'],
		],
		'coin_image'=> '/images/coinssmall/',
	];