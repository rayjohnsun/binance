<?php 
	
	session_start();

	define('LIBROOT', dirname(__FILE__).'/lib/');

	define('CLASSROOT', dirname(__FILE__).'/classes/');

	define('CONFIGROOT', dirname(__FILE__).'/config/');

	define('PARTROOT', dirname(__FILE__).'/parts/');

	$config = include_once CONFIGROOT.'app.php';

	include_once LIBROOT.'Functions.php';

	include_once CLASSROOT.'DB.php';

	include_once CLASSROOT.'Coins.php';

	DB::connect($config['db']);

	$list30 = $config['list_coin'];

	$coinimg= $config['coin_image'];

	$fsyms 	= implode(',', array_keys($list30));

	$Coins 	= new Coins();

	$Coins->open();

	$Coins->fsyms	= $fsyms;

	$Coins->tsyms 	= 'USD';

	$Coins->execute2();

	$Coins->close();

	$res = $Coins->getMulti(true);

	$nat = [];

	foreach ($list30 as $key => $value) {

		$nat[$key] = $value;

		$nat[$key]['price'] = $nat[$key]['up_dow'] = $nat[$key]['change'] = $nat[$key]['img'] = '';

		if (isset($res[$key])) {
			
			$nat[$key]['price'] = @$res[$key]['display']['PRICE'];

			$symbol = @$res[$key]['display']['TOSYMBOL'];

			$change = @$res[$key]['raw']['CHANGE24HOUR'];

			$nat[$key]['up_dow']= $change >= 0 ? 'up' : 'down';

			$nat[$key]['change']= number_format($change, 2, '.', '').$symbol;

			$nat[$key]['img'] = $coinimg . $value['icon'];

		}

	}

?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Universal Bitcoin</title>
<link href="/css/bootstrap.min.css" rel="stylesheet" />
<link type="text/css" href="/css/style.css" rel="stylesheet" />
<link type="text/css" href="/css/owl.carousel.min.css" rel="stylesheet" />
<link type="text/css" href="/css/slick.css" rel="stylesheet" />
<link type="text/css" href="/css/responsive.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet"> 
</head>
<!--[if lt IE 9]>
     <script src="/js/html5.js"></script>
 <![endif]-->
<body id="main" class="buy">


<ul class="soc soc-left">
	<li><a href="link" class="telegram"></a></li>
	<li><a href="link" class="fb"></a></li>
	<li><a href="link" class="tw"></a></li>
</ul>


<ul class="top-menu">
	<li><a href="#header">1</a></li>
	<li><a href="#about">2</a></li>
	<li><a href="#plus">3</a></li>
	<li><a href="#news">4</a></li>
	<li><a href="#articles">5</a></li>
	<li><a href="#sub">6</a></li>
</ul>


<header id="header">
	<div class="container">	
	
		<section>
		<ul class="lang">
			<li class="dropdown-li"><a href="link" class="ru">RU</a>
			
				<ul>
					<li><a href="link" class="ru">RU</a></li>
					<li><a href="link" class="eng">ENG</a></li>
				</ul>
			
			</li>
			
		</ul>
		
		<div class="search-top">
			
			<form action="" method="post" class="search white">
					<input type="text"  class="form-control" name="search" id="search-top" placeholder="Поиск по сайту ...">
					<input type="submit" class="search-btn" value="" />
			</form>
		</div>
	
		<div class="logo">
			
			<img src="images/main/logo-ub.png" alt="Universal Bitcoin" width="70" /><p>Universal<span>bitcoin</span></p>
		</div>
		</section>
		
	
		<div class="navbar-header">
		
			<nav class="navbar nav navbar-default">
			
				<div class="container-fluid">
			
					<div class="navbar-header">
						
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main">
        					<span class="icon-bar"></span>
        					<span class="icon-bar"></span>
        					<span class="icon-bar"></span>
      					</button>
						
						
					</div>
					
					<div class="collapse navbar-collapse" id="navbar-main">		
			<ul>
				<li class="dropdown-li">Где купить
				
				<ul>
					<li><a href="link">Gatecoin.com</a></li>
					<li><a href="link">Changelly.com</a></li>
					<li><a href="link">Localbitcoins.com</a></li>
					<li><a href="link">Coinmama.com</a></li>
					<li><a href="link">Paxful platform</a></li>
					<li><a href="link">Bitpanda.com</a></li>
					<li><a href="link">Coins.io</a></li>
					<li><a href="link">Coinhouse.io</a></li>
					<li><a href="link">Coinigy</a></li>
					<li><a href="link">Lakebtc</a></li>
					<li><a href="link">Coinbase.com </a></li>
				</ul>	
				
				</li>
				<li class="dropdown-li">Где трейдить
				
				<ul>
					<li><a href="link">binance</a></li>
					<li><a href="link">Kucoin.com</a></li>
					<li><a href="link">Bibox.com</a></li>
					<li><a href="link">cex.io</a></li>
					<li><a href="link">HitBtc.com</a></li>
					<li><a href="link">Cryptopia.com</a></li>
					<li><a href="link">Bitmex.com</a></li>
					<li><a href="link">exmo.com</a></li>
					<li><a href="link">Bittrex.com</a></li>
					<li><a href="link">cobinhood.com</a></li>
					<li><a href="link">Bitfinex.com</a></li>
				</ul>
				
				</li>
				<li>Использование</li>
				<li class="dropdown-li">Как майнить
				
				<ul>
					<li><a href="link">Облачный майнинг</a></li>
					<li><a href="link">Майнинг на Asic (Bitcoin+Ethereum)</a></li>
					<li><a href="link">Браузерный майнинг (Monero)</a></li>
					<li><a href="link">Вирусный майнинг</a></li>
				</ul>
					
				</li>
				<li class="dropdown-li">Как хранить
				<ul>
					<li><a href="link">Ico</a></li>
					<li><a href="link">Новости</a></li>
					<li><a href="link">Блог (статьи)</a></li>
					<li><a href="link">Полезные факты</a></li>
					<li><a href="link">Сравнения бирж</a></li>
				</ul>
				
				</li>
				<li>Применение</li>
				<li class="dropdown-li">О нас
				<ul>
					<li><a href="link">О компании</a></li>
					<li><a href="link">История розыгрышей</a></li>
				</ul>
				</li>
			</ul>
			</div>
					
					
				</div>	
			
			</nav>
		
		</div>
		
		<div class="row banner">
			<div class="col-sm-12 col-md-6">
				<h1>Помощь в построении реферальной сети</h1>
				<ul>
					<li>Анализ способов оплаты, где дешевле купить крипту</li>
					<li>Разбор видов верификации, скорость прохождения и документы для оформления</li>
					<li>Подробный курс от регистрации до покупки криптовалюты</li>
					<li>Получайте скидки и выигрывайте 1 биткоин в нашей реферальной системе</li>
				</ul>
				<a href="link" class="btn">Подробнее</a>
			</div>
			<div class="col-sm-12 col-md-5 col-md-offset-1">
			</div>
			
		</div>
		
	</div>
	<a href="#about" class="scroll"><img src="images/main/scroll.png"></a>
</header>

<section id="about" class="about">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6">
				
				<h2>Поможем выгодно купить криптовалюту за фиат</h2>
				<p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. </p>
				<p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. </p>
				
			</div>
			<div class="col-sm-12 col-md-6 text-center">
				
				<div class="video earth popup">
					<a href="link" data-toggle="modal" data-target="#modalvideo"><img src="images/main/bg_video_earth.png" /></a>
					<span class="time">0:45. О компании “Universal Bitcoin”</span>
				</div>
				
			</div>
			<div class="modal fade modal-call" id="modalvideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  	<div class="modal-dialog modal-md">
				    <div class="modal-content end">
				      	<div class="modal-body">
				      		<iframe width="560" height="315" src="https://www.youtube.com/embed/YQyrx6rpD48" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				      	</div>
				    </div>
			  	</div>
			</div>
		</div>
	</div>
</section>

<section id="plus" class="block exchanges">
	<div class="container">
		<div class="row">
			<h2 class="text-center">Подбор бирж</h2>
			<div class="col-sm-12 col-md-12">
				<div class="top"><p>Выберите способы оплаты криптовалюты?</p>
				
					<form action="" method="post" class="search white ui-widget">
						<input type="text"  class="form-control" name="search" id="search-exchanges" placeholder="Поиск...">
						<input type="submit" class="search-btn" value="" />
						
						<ul class="search-results" style="display: none;">
							<li><a href="link"><img src="images/main/icon-btn-small.png" width="15"> Bitcoin</a></li>
							<li><a href="link"><img src="images/main/icon-btn-small.png" width="15"> Bitcoin</a></li>
							<li><a href="link"><img src="images/main/icon-btn-small.png" width="15"> Bitcoin</a></li>
						</ul>
						
					</form>
				
				</div>
				
				<ul>
					<li><span class="logo-block"><img src="images/main/logo-gatecoin.jpg" alt="Gatecoin" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-changelly.jpg" alt="Changelly" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-local.jpg" alt="LocalBitcoins.com" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-coinmama.jpg" alt="Coinmama" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-paxful.jpg" alt="PAXFUL" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-bitpanda.jpg" alt="Bitpanda" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-coinhouse.jpg" alt="Coinhouse" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-coinigy.jpg" alt="CoinIGY" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-lakebtc.jpg" alt="LakeBTC" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
					<li><span class="logo-block"><img src="images/main/logo-coinbase.jpg" alt="Coinbase" /></span><p class="icon_plus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p><p class="icon_minus">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько</p>
					<div class="link"><a href="link" class="link_transform">Узнать подробнее</a></div>
					</li>
				</ul>
				
			</div>
		</div>
	</div>
</section>


<section id="news" class="block bg_blue">
	<div class="container">
		<div class="row">
			<h2 class="text-center">Последние новости<br />и цены криптовалют</h2>
			
			<div class="col-md-6 col-sm-12 news">
				
				<article class="icon_bch">
					<p>Обменник ShapeShift попадает в спор BTC/BCH из-за партнерства  с кошельком Bitcoin.com</p>
					<span>Популярный обменник ShapeShift был вовлечен в обсуждение по поводу сотрудничества с кошельком Bitcoin.Com, который обменивает BTC на BCH. ShapeShift заявила, что через твитты произошло недопонимание ....</span>
					<a href="link">Подробнее ...</a>
					<ins>5 часов назад</ins>
				</article>
				<article class="icon_eth">
					<p>В сообществе Ethereum продолжаются споры по предложению модифицировать Blockchain</p>
					<span>В сообществе Ethereum так и нет единого мнения по вопросам изменения Blockchain для возврата средств в случае кражи. ....</span>
					<a href="link">Подробнее ...</a>
					<ins>10 часов назад</ins>
				</article>
				<article class="icon_crpt">
					<p>Pantera Capital инвестирует в Cypherium</p>
					<span>Венчурный фонд Pantera Capital включил в свой инвестиционный портфель блокчейн-проект Cypherium.. ....</span>
					<a href="link">Подробнее ...</a>
					<ins>15 часов назад</ins>
				</article>
				<article class="icon_crpt">
					<p>Pantera Capital инвестирует в Cypherium</p>
					<span>Венчурный фонд Pantera Capital включил в свой инвестиционный портфель блокчейн-проект Cypherium.. ....</span>
					<a href="link">Подробнее ...</a>
					<ins>15 часов назад</ins>
				</article>
				<article class="icon_crpt">
					<p>Pantera Capital инвестирует в Cypherium</p>
					<span>Венчурный фонд Pantera Capital включил в свой инвестиционный портфель блокчейн-проект Cypherium.. ....</span>
					<a href="link">Подробнее ...</a>
					<ins>15 часов назад</ins>
				</article>
				
				
			</div>
			<div class="col-md-6 col-sm-12">
				
				<div class="currency">
					<div class="currency-table">
						<div class="th">
							<div style="width: 49%;">Криптовалюта <span>Наименование</span></div>
							<div style="width: 19%;">Цена <span>USD</span></div>
							<div style="width: 29%;">Изменения <span>За 24 часа</span></div>
						</div>
						
						<div class="currency-js" style="padding-bottom: 40px;">
						
							<?php if (!empty($nat)): ?>

								<?php foreach ($nat as $key2 => $value2): ?>
									
									<div class="td" style="height: 93px;">

										<div style="width: 49%;"><img src="<?=$value2['img'] ?>" style="width: 27px;height: 27px;"><?=$value2['id'] ?></div>

										<div style="width: 19%;"><?=$value2['price'] ?></div>

										<div style="width: 29%;">

											<span class="<?=$value2['up_dow'] ?>"><?=$value2['change'] ?></span>

										</div>

									</div>

								<?php endforeach ?>

							<?php endif ?>
						
						</div>
					
					</div>
				</div>
				
				
			</div>
			
		</div>
	</div>
</section>

<section id="articles" class="block map articles">
	<div class="container">
		<div class="row">
			<h2 class="text-center">Обзоры и статьи</h2>
			<div class="col-sm-12 col-md-4">
			<article class="item">
			<a href="link"><img src="images/main/main-img-5.jpg" /></a>
			<p><a href="link">Pantera Capital инвестирует в Cypherium</a></p>
			<span><a href="link">Венчурный фонд Pantera Capital включил в свой инвестиционный портфель блокчейн-проект Cypherium.. ....</a></span>
			<a href="link">Подробнее ...</a>
			<ins>15 часов назад</ins>
			</article>
			</div>
			
			<div class="col-sm-12 col-md-4">
			<article class="item">
			<a href="link"><img src="images/main/main-img-6.jpg" /></a>
			<p><a href="link">Pantera Capital инвестирует в Cypherium</a></p>	
			<span><a href="link">Венчурный фонд Pantera Capital включил в свой инвестиционный портфель блокчейн-проект Cypherium.. ....</a></span>
			<a href="link">Подробнее ...</a>
			<ins>15 часов назад</ins>
			</article>
			</div>
			<div class="col-sm-12 col-md-4">
			<article class="item">
			<a href="link"><img src="images/main/main-img-7.jpg" /></a>
			<p><a href="link">Pantera Capital инвестирует в Cypherium</a></p>	
			<span><a href="link">Венчурный фонд Pantera Capital включил в свой инвестиционный портфель блокчейн-проект Cypherium.. ....</a></span>
			<a href="link">Подробнее ...</a>
			<ins>15 часов назад</ins>
			</article>	
			</div>
		</div>
	</div>
</section>

<section id="sub" class="block sub">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-md-offset-1">
			<form action="" method="post">
			<p>Подпишитесь!</p>
			<span>И будьте в курсе новостей криптовалютного рынка</span>
			<input class="form-control" name="name" id="name-sub" placeholder="Имя" type="text">
					<input class="form-control" name="address" id="address-sub" placeholder="E-mail" type="text">
					<button type="submit">Получать новости первым</button>
			</form>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-3">
				<p>Где купить <ins>?</ins></p>
				<ul>
					<li><a href="link">Gatecoin.com</a></li>
					<li><a href="link">Changelly.com</a></li>
					<li><a href="link">localbitcoins.com</a></li>
					<li><a href="link">Coinmama.com</a></li>
					<li><a href="link">Paxful platform</a></li>
					<li><a href="link">Bitpanda.com</a></li>
					<li><a href="link">Coins.io</a></li>
					<li><a href="link">Coinhouse.io</a></li>
					<li><a href="link">Coinigy</a></li>
					<li><a href="link">Lakebtc</a></li>
					<li><a href="link">Coinbase.com </a></li>
				</ul>
				
			</div>
			<div class="col-sm-12 col-md-3">
				<p>Где трейдить <ins>?</ins></p>
				<ul>
					<li><a href="link">binance</a></li>
					<li><a href="link">Kucoin.com</a></li>
					<li><a href="link">Bibox.com</a></li>
					<li><a href="link">cex.io</a></li>
					<li><a href="link">HitBtc.com</a></li>
					<li><a href="link">Cryptopia.com</a></li>
					<li><a href="link">Bitmex.com</a></li>
					<li><a href="link">exmo.com</a></li>
					<li><a href="link">Bittrex.com</a></li>
					<li><a href="link">cobinhood.com</a></li>
					<li><a href="link">Bitfinex.com</a></li>
				</ul>
				
				
			</div>
			<div class="col-sm-12 col-md-3">
				<p>Дебетовые карты</p>
				<ul>
					<li><a href="link">как покупать товары и снимать фиат моментально</a></li>
				</ul>
				
				<p>Как майнить <ins>?</ins></p>
				<ul>
					<li><a href="link">Облачный майнинг</a></li>
					<li><a href="link">Майнинг на Asic (Bitcoin+Ethereum)</a></li>
					<li><a href="link">Браузерный майнинг (Monero)</a></li>
					<li><a href="link">Вирусный майнинг</a></li>
				</ul>
				
				
			</div>
			<div class="col-sm-12 col-md-3">
				
				<p>Как хранить <ins>?</ins></p>
				<ul>
					<li><a href="link">Ico</a></li>
					<li><a href="link">Новости</a></li>
					<li><a href="link">Блог (статьи)</a></li>
					<li><a href="link">Полезные факты</a></li>
					<li><a href="link">Сравнения бирж</a></li>
				</ul>
				
				<p>О нас <ins>?</ins></p>
				<ul>
					<li><a href="link">О компании</a></li>
					<li><a href="link">История розыгрышей</a></li>
				</ul>
				
				
				
			</div>
		</div>
		
		<div class="row footer-bottom">
			<div class="col-sm-12 col-md-4 soc">
				
				<p>Мы в соц сетях:</p>
				<a href="link" class="telegram"></a><a href="link" class="fb"></a><a href="link" class="tw"></a>
				
			</div>
			<div class="col-sm-12 col-md-4 text-center">
				
				<div class="logo">
			
					<img src="images/main/logo-ub.png" alt="Universal Bitcoin" width="70" /><p>Universal<span>bitcoin</span></p>
				</div>
				
			</div>
			<div class="col-sm-12 col-md-4 text-right">
				
				<form action="" method="post" class="search">
					<input type="text"  class="form-control" name="search" id="search" placeholder="Поиск по сайту ...">
					<input type="submit" class="search-btn" value="" />
				</form>
				
			</div>
		</div>
		
	</div>
</footer>


<script src="/js/jquery-3.2.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/main.js"></script>


<script>
$('.news').slick({
  infinite: true,
	vertical: true,
  slidesToShow: 3,
  slidesToScroll: 1
});
	</script>
<script>
$('.currency-js').slick({
  infinite: true,
	vertical: true,
  slidesToShow: 6,
  slidesToScroll: 1
});
	</script>
        
<script>
	// $(document).ready(function(){
	// 	$(".top-menu").changeActiveNav();
	// });
</script>
 
        	        
</body>
</html>
