$("body").on('click', '[href*="#"]', function(e){
  var fixed_offset = 100;
  $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
  e.preventDefault();
});


$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:7
        }
    }
})
	
		// Ждем когда загрузится DOM
$(function() {
  // Инициализация плагина на форме
  // Форма имеет атрибут "registration"
  $("form[name='creatform']").validate({
    // Правила проверки полей
    rules: {
      // Ключ с левой стороны это название полей формы.
      // Правила валидации находятся с правой стороны
      login: "required",
      address: "required",
		email: {
        required: true,
        // Отдельное правило для проверки email
        email: true
      }      
     
    },
    // Установка сообщений об ошибке
    messages: {
      login: "Please enter your login",
      address: "Please enter your wallet address",
		email: "Please enter your email"
      
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});


var ctx = document.getElementById("myChart");
if (ctx) {
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["0","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","1","2","3"],
            datasets: [{
                label: 'December - January',
                data: [0, 0.2, 0.3, 0.5, 0.6, 0.7, 0.75, 0.80, 0.85, 0.90, 0.95, 1.1, 1.2, 1.35, 1.45, 1.45, 1.5, 1.5, 1.45, 1.45, 1.45, 1.45, 1.5, 1.5, 1.6, 1.75, 2, 2.25, 2.3, 2.2, 2.4],
                backgroundColor: [
                    'rgba(103, 93, 148, 1)'
                ],
                borderColor: [
                    'rgba(245,245,245,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}


var ctx2 = document.getElementById("myChart2");
if (ctx2) {
    var myChart = new Chart(ctx2, {
        type: 'line',	
        data: {
            labels: ["Jan","March","May","July","Sep","Nov","Jan"],
            datasets: [{
                label: 'December - January',
                data: [0, 430, 500, 520, 550, 620, 750],
                backgroundColor: [
                    'rgba(103, 93, 148, 1)'
                ],
                borderColor: [
                    'rgba(245,245,245,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}



